const BASE_URL = "https://636652cd046eddf1bafd7a48.mockapi.io/";

export let addToCart = (id) => {
    return axios({
        url:`${BASE_URL}/cart/${id}`,
        method: "POST",
    })
}