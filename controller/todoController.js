render = (item) => {
    var contentHTML="";
    item.forEach((data) => {
        let {name,type,price,screen, backCamera,frontCamera,img,desc,id} = data;
        var contentTr =`
        <div class="item-card col-sm-6 col-lg-4 mt-lg-0">
        <div class="nameproduct">${name}</div>
        <div class="loaidt" id="type">${type}</div>
        <div class="giadt" id="gia">${price}</div>
        <div class="item-desc">${screen}</div>
        <div>Back Camera: ${backCamera}</div>
        <div>Front Camera: ${frontCamera}</div>
        <div class="img-container"><img class="imgcon" src="${img}"></div>
        <div class="item-desc" id="desc">${desc}</div>
        <div class="item-button" align="center">
        <button onclick="addToCart(${id})" class="btn btn-primary shop-item-button w-50 p-3" >Thêm vào giỏ hàng</button></div>
        </div>`;
        contentHTML += contentTr
    });
    return document.getElementById("rowcon").innerHTML = contentHTML;
    ;
};
function turnOnLoading(){
    document.getElementById("loading").style.display = 
"flex";
}

function turnOffLoading(){
    document.getElementById("loading").style.display = 
"none";
}

function layThongTinTuForm(){
    var type = document.getElementById("phonetype").value;
    return {
        type: type,
    }
}
