var sanPhamSer = new SanPhamService();

function getELE(id){
    return document.getElementById(id);
}

getListProducts();
function getListProducts(){
    sanPhamSer.danhsachSanPham()
    .then(function(result){
        console.log(result.data);
        renderTable(result.data);
        setLocalStorage(result.data);
    })
    .catch(function(error){
        console.log(error);
    });
}
function setLocalStorage(mangSP){
    localStorage.setItem("DSSP",JSON.stringify(mangSP));
}

getELE("basic-addon2").addEventListener("click",function(){
    var mangSP = getLocalStorage();
    var mangTK = [];
    console.log(mangSP);

    var chuoiTK = getELE("inputTK").value;
    
    mangTK = sanPhamSer.timKiemSP(mangSP,chuoiTK);

    console.log(mangTK);
    renderTable(mangTK);

});

function getLocalStorage(){
    var mangKQ = JSON.parse(localStorage.getItem("DSSP"));
    return mangKQ
}


getELE("btnThemSP").addEventListener("click",function(){
    var footerEle = document.querySelector(".modal-footer");
    footerEle.innerHTML = `
        <button onclick="addProducts()" class="btn btn-success">Add Product</button>
    `;
});

function renderTable(mangSP){
    var content = "";
    var count = 1;
    mangSP.map(function(sp,index){
        content += `
            <tr>
                <td>${count}</td>
                <td>${sp.name}</td>
                <td>${sp.price}</td>
                <td><img src="${sp.img}" width="100px"></td>
                <td>${sp.desc}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaSP('${sp.id}')">Xóa</button>
                    <button class="btn btn-info" onclick="xemSP('${sp.id}')">Xem</button>
                </td>
            </tr>
        `;
        count++;
    });
    getELE("tblDanhSachSP").innerHTML = content;
}

function addProducts(){
    var name = getELE("Name").value;
    var price = getELE("Price").value;
    var img = getELE("Img").value;
    var desc = getELE("Desc").value;

    var sp = new SanPham(name,price,img,desc);
    console.log(sp);

    sanPhamSer.themSP(sp)
    .then(function(result){   
         getListProducts();
         document.querySelector("#myModal .close").click();
    })
    .catch(function(error){
        console.log(error);
    });


}

function xoaSP(id){
    sanPhamSer.xoaSanPham(id)
    .then(function(result){
         getListProducts();
        
    })
    .catch(function(error){
        console.log(error);
    });

}

function xemSP(id){
    sanPhamSer.xemSanPham(id)
    .then(function(result){
        console.log(result.data);
        $('#myModal').modal('show');
        //Điền thông tin lên form
        getELE("TenSP").value  = result.data.name;
        getELE("GiaSP").value = result.data.price;
        getELE("HinhSP").value = result.data.img;
        getELE("MoTa").value = result.data.desc;
        var footerEle = document.querySelector(".modal-footer");
        footerEle.innerHTML = `
            <button onclick="capNhatSP('${result.data.id}')" class="btn btn-success">Update Product</button>
        `;

    })
    .catch(function(error){
        console.log(error);
    });

}

function capNhatSP(id){
    var name = getELE("Name").value;
    var price = getELE("Price").value;
    var img = getELE("Img").value;
    var desc = getELE("Desc").value;

    var sp = new SanPham(name,price,img,desc);
    console.log(sp);

    sanPhamSer.capNhatSanPham(id,sp)
    .then(function(result){
        console.log(result.data);
         getListProducts();
         document.querySelector("#myModal .close").click();
    })
    .catch(function(error){
        console.log(error);
    });

}

