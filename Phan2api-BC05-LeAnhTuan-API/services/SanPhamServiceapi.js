const BASE_URL = "https://636652cd046eddf1bafd7a48.mockapi.io/";


function SanPhamService() {
    this.danhsachSanPham = function () {
        var promise = axios({
            method:'get',
            url: `${BASE_URL}/cart`
        });
        return promise;
    }
    this.themSP = function (sp) {
        var promise = axios({
            method: 'post',
            url: `${BASE_URL}/cart`,
            data: sp
        });

        return promise;
    }
    this.xoaSanPham = function (id) {
        var promise = axios({
            method: 'delete',
            url: `${BASE_URL}/cart/${id}`
        });

        return promise;
    }
    this.xemSanPham = function (id) {
        var promise = axios({
            method: 'get',
            url: `${BASE_URL}/cart/${id}`
        });

        return promise;
    }
    this.capNhatSanPham = function (id, sp) {
        var promise = axios({
            method: 'put',
            url: `${BASE_URL}/cart/${id}`,
            data: sp
        });
        return promise;
    }


    this.timKiemSP = function(mangSP, chuoiTK){

        var mangTK = [];
        mangTK = mangSP.filter(function(sp){
            return sp.tenSP.toLowerCase().indexOf(chuoiTK.toLowerCase()) >= 0;
        });
        return mangTK;
    }


}