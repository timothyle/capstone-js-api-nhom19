const BASE_URL = "https://636652cd046eddf1bafd7a48.mockapi.io/";

function fetchAllTodo(){
    // render all todos service
    turnOnLoading();
    axios({
    url: `${BASE_URL}/CapStoneLeAnhTuanBC05` ,
    method:"GET",
})
.then(function(res){
    turnOffLoading();
    render(res.data);
    return res.data;
})
.catch(function(err){
    turnOffLoading();
    console.log("err:",err);
});
}
fetchAllTodo ();
function hienThiTypeSamsung(){
    turnOnLoading();
    let phoneType = axios.get('https://636652cd046eddf1bafd7a48.mockapi.io/CapStoneLeAnhTuanBC05').then(res => res.data)
    phoneType.then(function(result){
    let typeS = [result[1],result[2]];
    render(typeS)
    turnOffLoading();
})};
function hienThiTypeIphone(){
    turnOnLoading();
    let phoneType = axios.get('https://636652cd046eddf1bafd7a48.mockapi.io/CapStoneLeAnhTuanBC05').then(res => res.data)
    phoneType.then(function(result){
    let typeI = [result[0],result[3]];
    render(typeI);
    turnOffLoading();
})
return phoneType}
;
var cartItems = [];

function addToCart(id){
    var cartItems = [];
    var price = document.getElementsByClassName('giadt')[id-1].innerHTML;
    var tendt = document.getElementsByClassName('nameproduct')[id-1].innerHTML;
    var hinhdt = document.getElementsByClassName('imgcon')[id-1].src;
    var quantity = 1;
    if(localStorage.getItem('cartItem')){
        cartItems = JSON.parse(localStorage.getItem('cartItem'));
    }
    var thongtindt = {price,tendt,hinhdt,quantity,id};
    for (var id = 0; id < cartItems.length; id++){
        if (cartItems[id].tendt == tendt) {
          alert('Item already in cart')
          return
        }
    }
    cartItems.push(thongtindt);
    localStorage.setItem('cartItem', JSON.stringify(cartItems));
    renderCart(cartItems);
    window.location.reload();
}

;
function renderCart(cartItems){
    var cartItems = JSON.parse( localStorage.getItem('cartItem'));
    if (cartItems !== null){
    var contentHTML="";
    for (var index = 0; index < cartItems.length; index++){
        var thongtindt = cartItems[index];
        var contentTr =`<div><tr>
        <td>${thongtindt.tendt}</td>
        <td>${thongtindt.price}</td>
        <td><img src="${thongtindt.hinhdt}" width="100px" height="100px"></td>
        <td><button class="p-2 mt-3 btn badge badge-secondary ml-auto" onclick="increase(${thongtindt.id})">&plus;</td>
        <span class="p-2 mt-3 text-success cart_item_quantity" id="soluongdt" value="${thongtindt.quantity}">${thongtindt.quantity}</span>
        <td><button class="btn badge badge-info p-2 mt-3" onclick="decrease(${thongtindt.id})">&minus;</td>
        <td><button onclick="remove(${thongtindt.id})" class="btn btn-warning">Remove</button></td>
        </tr></div>`;
        tonggia = thongtindt.price* thongtindt.quantity;
        contentHTML += contentTr;
    }};
    document.getElementById("thongTinCart").innerHTML = contentHTML;
    calculateSum(cartItems);
};

function increase(id){
    var cartItems = JSON.parse(localStorage.getItem('cartItem'));
    console.log("cartItems",cartItems);
    for (var index = 0; index < cartItems.length; index++){
        if(cartItems[index].id == id){
        var thongtindt = cartItems[index];
        thongtindt.quantity += 1
    }}; 
    localStorage.setItem('cartItem', JSON.stringify(cartItems));
    renderCart(cartItems);
};
function decrease(id){
    var cartItems = JSON.parse(localStorage.getItem('cartItem'));
    console.log("cartItems",cartItems);
    for (var index = 0; index < cartItems.length; index++){
        var thongtindt = cartItems[index];
        if (thongtindt.quantity < 2){
            alert('Số lượng không được bé hơn 1')
            thongtindt.quantity = 1;
        }
        if(cartItems[index].id == id){
            thongtindt.quantity -= 1
        }}; 
    localStorage.setItem('cartItem', JSON.stringify(cartItems));
    renderCart(cartItems);
};

function remove(id) {
    // remove product from cart (and from local storage)
    // retrieve list of products from LS
    var cartItems = JSON.parse(localStorage.getItem('cartItem'));
    // get the index of the product item to remove
    // inside the local storage content array
    let productIndex;
    cartItems.forEach(function(remove, i) {
      if (remove.id === id) {
        productIndex = i;
      }
    });
    // modify the items in local storage array
    // to remove the selected product item
    cartItems.splice(productIndex, 1);
    // update local storage content
    localStorage.setItem('cartItem', JSON.stringify(cartItems));
    window.location.reload();
    renderCart(cartItems);
}
  var cartItems = JSON.parse(localStorage.getItem('cartItem'));
  console.log("cartItems",cartItems)
  
function calculateSum(cartItems){
    var cartItems = JSON.parse(localStorage.getItem('cartItem'));
    var sum = 0;
    for (var id = 0; id < cartItems.length; id++){
          var gia = parseInt(cartItems[id].price);
          var soluong = cartItems[id].quantity;
          tong = gia*soluong;
          sum += tong;
    }
    document.getElementById("tongHD").innerHTML ="$"+ sum;
}

function purchase(cartItems){
    var cartItems = JSON.parse(localStorage.getItem('cartItem'));
    let length = cartItems.length;
    console.log("length",length);
    let index = 0;
    cartItems.splice(index,length);
    localStorage.setItem('cartItem', JSON.stringify(cartItems));
    window.location.reload();
    renderCart(cartItems);
}
renderCart(cartItems);